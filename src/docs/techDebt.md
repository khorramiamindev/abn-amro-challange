# Tech Debt

Just want to mention, in case of more time I would implement these features.
1. By having the DTO of api calls, more accurate types for request and response of the https://www.tvmaze.com/api will be implemented.
2. Adding a proper pagination in order to decrease the size of the query. (now there's no query size set on the api call which should be more customisable.)
3. The search feature needs to be implemented to simulate a more real world application for this assignment. ( An input with a limitation of 4, 5 character and proper validation of characters on the UI side and an api call mechanism to trigger the call on the network side, will be my approach).
4. The CSS is not great and though I tried to use couple of options to show you my abilities there. It requires more time and efforts to make it better for sure.
5. Without coverage test either unit or e2e, I'm not confident about the app. Here the next important feature I will add would be proper testing.