# Vue 3 + TypeScript + Vite

1. React has been my first tool in the past couple of years, but to demonstrate my abilities I went with Vue3 and Typescript, which required more effort on my part. ( Vuex, vue-router, new composition architecture of vue, etc.)
2. Vuex for handling the global state is the first priority from my understanding in the community and I find it quite clear on the flow.
3. Vue-Router is quite well defined and I followed the same pattern from the docs.