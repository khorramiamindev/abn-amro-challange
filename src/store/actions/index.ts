import { ActionTree } from "vuex";

import MutationTypes from "../mutations/mutation-types";
import ActionTypes from "./action-types";

import { ActionsTypeInterface } from "../types";

import fetchShowById from "@services/api/detail";
import fetchShowsList from "@services/api/list";

const actions: ActionTree<any, any> & ActionsTypeInterface = {
  async [ActionTypes.FETCH_SHOWS_LIST]({ commit }): Promise<void> {
    commit(MutationTypes.FETCH_SHOWS_LIST);
    try {
      const response: unknown = await fetchShowsList();
      commit(MutationTypes.FETCH_SHOWS_SUCCEED, response);
    } catch (error) {
      commit(MutationTypes.FETCH_SHOWS_FAILED);
    }
  },
  async [ActionTypes.FETCH_SHOW_BY_ID]({ commit }, payload): Promise<void> {
    const { showId } = payload;
    commit(MutationTypes.FETCH_SHOW_BY_ID);
    try {
      const response: unknown = await fetchShowById(showId);
      commit(MutationTypes.FETCH_SHOW_BY_ID_SUCCEED, response);
    } catch (error) {
      commit(MutationTypes.FETCH_SHOW_BY_ID_FAILED);
    }
  },
};

export default actions;
