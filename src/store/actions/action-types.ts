enum ActionTypes {
    FETCH_SHOWS_LIST = "FETCH_SHOWS_LIST",
    FETCH_SHOW_BY_ID = "FETCH_SHOW_BY_ID",
  }

  export default ActionTypes;
