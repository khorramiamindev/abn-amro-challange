const initialState: any = {
  list: {
    data: null,
    isLoading: false,
  },
  details: {
    data: null,
    isLoading: false,
  },
};

export default initialState;
