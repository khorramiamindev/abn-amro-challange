import initialState from "./initialState";
import { InjectionKey, State } from "vue";
import { Store } from "vuex";

const defaultState: State = {
  ...initialState
};

// define injection key
export const key: InjectionKey<Store<State>> = Symbol();

export default defaultState;