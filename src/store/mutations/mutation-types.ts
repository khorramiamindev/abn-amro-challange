enum MutationTypes {
    FETCH_SHOWS_LIST = "FETCH_SHOWS_LIST",
    FETCH_SHOWS_SUCCEED = "FETCH_SHOWS_SUCCEED",
    FETCH_SHOWS_FAILED = "FETCH_SHOWS_FAILED",

    FETCH_SHOW_BY_ID = "FETCH_SHOW_BY_ID",
    FETCH_SHOW_BY_ID_SUCCEED = "FETCH_SHOW_BY_ID_SUCCEED",
    FETCH_SHOW_BY_ID_FAILED = "FETCH_SHOW_BY_ID_FAILED",
  }

export default MutationTypes;
