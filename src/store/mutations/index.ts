import MutationTypes from "./mutation-types";
import {  MutationsTypeInterface } from "../types";
import { MutationTree } from "vuex";

const mutations: MutationTree<any> & MutationsTypeInterface = {
  [MutationTypes.FETCH_SHOWS_LIST](state) {
    state.list.isLoading = true;
  },

  [MutationTypes.FETCH_SHOWS_SUCCEED](state, payload) {
    state.list.isLoading = false;
    state.list.data = payload;
  },
  [MutationTypes.FETCH_SHOWS_FAILED](state) {
    state.list.isLoading = false;
  },

  [MutationTypes.FETCH_SHOW_BY_ID](state) {
    state.details.isLoading = true;
  },

  [MutationTypes.FETCH_SHOW_BY_ID_SUCCEED](state, payload) {
    state.details.isLoading = true;
    state.details.data = payload;
  },
  [MutationTypes.FETCH_SHOW_BY_ID_FAILED](state) {
    state.details.isLoading = true;
  },
};

export default mutations;
