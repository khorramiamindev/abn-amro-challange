
import { State } from "vue";
import { createStore, createLogger } from "vuex";
import defaultState from "./state";
import state from "./state";
import getters from "./getters";
import actions from "./actions";
import mutations from "./mutations";

const features = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};


const store = createStore<State>({
  state: defaultState,
  plugins: [createLogger()],
  modules: {
    shows: features,
  },
});

export default store;
