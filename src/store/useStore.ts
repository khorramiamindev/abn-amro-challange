import { useStore as baseUseStore } from "vuex";
import { key } from "./state";

const useStore = () => baseUseStore(key);

export default useStore;
