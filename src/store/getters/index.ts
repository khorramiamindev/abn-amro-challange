import { GettersType } from "../types";

const getters: GettersType = {
  selectedShowsList: (state) => state.list,
  selectedShowByIdDetails: (state) => state.details,
};

export default getters;
