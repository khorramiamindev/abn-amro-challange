import { Commit } from "vuex";
import MutationTypes from "../mutations/mutation-types";
import ActionTypes from "../actions/action-types";

interface TShowsList {
  // I don't have the DTO so I didn't implement the exact types
  data: any;
  isLoading: boolean;
}

// init State Types:
type TShowModuleState = {
  list: TShowsList;
  details: TShowsList;
};

// default VUEX Types
interface ActionTypeDefault {
  commit: Commit;
}

// action Types
interface TFetchShowByIdPayload {
  showId: string;
}

// MutationTypes
type MutationsTypeInterface<S = TShowModuleState> = {
  [MutationTypes.FETCH_SHOW_BY_ID](state: S): void;
  [MutationTypes.FETCH_SHOW_BY_ID_SUCCEED](state: S, payload: unknown): void;
  [MutationTypes.FETCH_SHOW_BY_ID_FAILED](state: S): void;
};

// default VUEX Types
interface ActionTypeDefault {
  commit: Commit;
}

// actions
interface ActionsTypeInterface {
  [ActionTypes.FETCH_SHOWS_LIST](
    { commit }: ActionTypeDefault,
    payload: any
  ): Promise<any>;
  [ActionTypes.FETCH_SHOW_BY_ID](
    { commit }: ActionTypeDefault,
    payload: TFetchShowByIdPayload
  ): Promise<any>;
}

// getters
type GettersType = {
  selectedShowsList(state: TShowModuleState): unknown;
  selectedShowByIdDetails(state: TShowModuleState): unknown;
};

export { ActionsTypeInterface, MutationsTypeInterface, GettersType };

export default TShowModuleState;
