import { createApp } from "vue";
import App from "./App.vue";
import router from "@router/index";
import store from "@store/index";
import { key } from "@store/state";

const app = createApp(App);
app.use(router);
app.use(store, key);
app.mount("#app");