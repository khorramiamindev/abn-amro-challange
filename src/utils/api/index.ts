import * as Axios from "axios";
import { HttpMethods } from "./types";

class HTTPClientCreator {
  private _baseUrl: string | undefined;

  /**
   * Create a new Http Client
   */
  constructor(baseUrl: string | undefined) {
    this._baseUrl = baseUrl;
  }

  /**
   * Return base URL of the current Http Client
   */
  public getBaseUrl() {
    return this._baseUrl;
  }

  /**
   * The method to create endPoint calls
   * @param method - Use from enum types
   * @param url - Url string that should be appended to the base url
   * @param data - In case, we want to send any data (POST, PATCH, PUT)
   */
  public request<D, P>(
    method: HttpMethods,
    url: string,
    data?: D,
    params?: P
  ): Promise<Axios.AxiosResponse> {
    return Axios.default.request({
      baseURL: this._baseUrl,
      method,
      url,
      headers: {
        // for auth handling we can put the config here
      },
      data,
      params,
    });
  }
}

export default HTTPClientCreator;
