// vuex.d.ts
import { Store } from "@/vuex";
import { TAppStateType } from "store/types";

declare module "@vue/runtime-core" {
  // declare your own store states
  type State = TAppStateType;

  // provide typings for `this.$store`
  interface ComponentCustomProperties {
    $store: Store<State>;
  }
}
