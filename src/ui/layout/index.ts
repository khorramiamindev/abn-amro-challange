import Footer from './footer/index.vue';
import Main from './main/index.vue';
import Header from './header/index.vue';
import Navbar from './navbar/index.vue';


export {Footer, Main, Header, Navbar};