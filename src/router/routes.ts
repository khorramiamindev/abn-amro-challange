import { RouteRecordRaw } from "vue-router";
import paths from "./appPaths";

const routes: Array<RouteRecordRaw> = [
  {
    path: paths.index,
    name: "Home",
    component: () =>
      import(
        /* webpackChunkName: "Home" */ "../views/home/index.vue"
      ),
  },
  {
    path: paths.showDetails,
    name: "showDetails",
    component: () =>
      import(
        /* webpackChunkName: "showDetails" */ "../views/details/index.vue"
      ),
  },
];

export default routes;
