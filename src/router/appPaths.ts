const paths : {index : string, showDetails: string} = {
    index: "/",
    showDetails: "/show/:id",
  };

export default paths;