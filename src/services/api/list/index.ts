import { HttpMethods } from "@utils/api/types";
import defaultClient from "../client";

/**
 * Returns the list of shows
 * @param {string} genre genre
 * @return {Array} return the list of shows
 */

type TFetchTVShows = () => any; // TODO: determining the type of req/res of this promise will be beneficial here
const fetchShowsList: TFetchTVShows = () => {
  return defaultClient.request(HttpMethods.GET, "/shows");

}

export default fetchShowsList;
