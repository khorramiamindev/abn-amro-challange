import { HttpMethods } from "@utils/api/types";
import defaultClient from "../client";

//TODO:
// 1. The validation could happen in the base case for api calls
// 2. The types for api could also be added
/**
 * Returns the details of show by ID
 *
 * @param {string} showId id of the show that we want to display
 * @return {Array} return the show detauls
 */

type TFetchTVShows = (showId: string) => any;
const fetchShowById: TFetchTVShows = (showId) => {
  if (!showId) {
    return;
  }
  return defaultClient.request(HttpMethods.GET, `/shows/${showId}`);
};

export default fetchShowById;
