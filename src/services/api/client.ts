import HttpClient from "@utils/api/index";

console.log('import.meta.env' , import.meta.env.VITE_APP_BASE_API_URL)
const defaultHttpClient = new HttpClient(import.meta.env.VITE_APP_BASE_API_URL);

export default defaultHttpClient;
